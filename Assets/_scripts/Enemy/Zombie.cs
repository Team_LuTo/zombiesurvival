﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Zombie : Unit
{
    [System.Serializable]
    public class ZombieProperties
    {
        [Header("Movement")]
        public float DirectionModifier; // TODO: Add randomness to zombie movement

        [Header("Attack")]
        public int Damage;
        public int AttackInterval;
        public int AttackRange;
        public float CurAttackTime;
        [NonSerialized]
        public float AttackTime;
    }

    [SerializeField]
    protected ZombieProperties zombieProperties;
    [Space(10)]
    private GameObject _target;

    void Start()
    {
        unitProperties.Sprite = GetComponentInChildren<SpriteRenderer>();
        _target = GameObject.FindGameObjectWithTag("Player");
    }

    void Awake()
    {
        unitProperties.Health = unitProperties.MaxHealth;
        zombieProperties.AttackTime = 1.0f / zombieProperties.AttackInterval;
        zombieProperties.CurAttackTime = zombieProperties.AttackTime;

        _healthBar = Instantiate(HealthBarPrefab, gameObject.transform) as Healthbar;
        _healthBar.SetTarget(this);

        unitMovement = GetComponent<UnitMovement>();
    }

    public override void Update()
    {
        base.Update();

        if (unitProperties.Dead)
        {
            Destroy(gameObject);
        }

        Vector2 targetPos = _target.transform.position;

        #region Movement

        if (Vector2.Distance(targetPos, transform.position) > unitProperties.Sprite.size.x)
        {
            Vector2 moveDir = transform.position;
            moveDir -= targetPos;

            unitMovement.Move(-moveDir);
        }

        #endregion Movement

        #region Rotation

        Vector2 lookDir = transform.position;
        lookDir -= targetPos;
        lookDir = -lookDir;
        var angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90;
        unitProperties.Sprite.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        #endregion Rotation

        #region Combat
        zombieProperties.CurAttackTime -= Time.deltaTime;

        if (Vector2.Distance(targetPos, transform.position) <= zombieProperties.AttackRange)
        {
            Attack();
        }

        #endregion Combat
    }


    public virtual void Attack()
    {
        if (zombieProperties.CurAttackTime <= 0)
        {
            _target.GetComponent<PlayerController>().TakeDamage(zombieProperties.Damage);

            zombieProperties.CurAttackTime = zombieProperties.AttackTime;
        }
    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);

    }
}
