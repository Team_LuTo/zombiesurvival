﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healthbar : MonoBehaviour
{
    public float BarWidth;

    [SerializeField]
    private SpriteRenderer _foreGround;
    [SerializeField]
    private SpriteRenderer _backGround;

    private float _healthPercent;
    private float _yOffset = 0.9f;
    private Unit _target;

    void Awake()
    {
        gameObject.transform.position = new Vector2(gameObject.transform.position.x, _yOffset);
        _foreGround.size = new Vector2(BarWidth, _foreGround.size.y);
        _backGround.size = new Vector2(BarWidth, _backGround.size.y);
    }

    public void SetTarget(Unit target)
    {
        _target = target;
    }

    void LateUpdate()
    {
        if (_target == null)
            return;

        _healthPercent = _target.GetHealthPercent();
        if (_healthPercent <= 0)
            return;

        float width = BarWidth * _healthPercent;
        float posX =  _backGround.transform.position.x - (BarWidth * 0.5f * (1 - _healthPercent));
        _foreGround.size = new Vector2(width, _foreGround.size.y);
        _foreGround.transform.position = new Vector2(posX, _backGround.transform.position.y);

        _foreGround.enabled = _healthPercent < 1.0f;
        _backGround.enabled = _healthPercent < 1.0f;
    }
}
