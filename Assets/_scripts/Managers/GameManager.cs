﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Manager : MonoBehaviour
{
    public abstract void Initialize(object Data);
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    private List<Manager> m_Managers;

    //Manager Data
    [SerializeField] private AudioManager.Data AudioManagerSettings;
    [SerializeField] private PlayerManager.Data PlayerManagerSettings;

    public T Get<T>() where T : Manager
    {
        foreach (Manager manager in m_Managers)
            if (manager.GetType() == typeof(T))
                return manager as T;

        Debug.LogError("Could not return manager of type \"" + typeof(T).Name + "\"");
        return null;
    }

    private void Initialize()
    {
        m_Managers = new List<Manager>();

        //Create other managers
        InitManager<AudioManager>(AudioManagerSettings);
        InitManager<PlayerManager>(PlayerManagerSettings);
    }

    private void Awake()
    {
        //Singleton behaviour
        if (Instance)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;

            Initialize();
        }
    }

    //Creates and initializes a new Manager
    private void InitManager<T>(object Settings) where T : Manager
    {
        Manager newManager = new GameObject(typeof(T).Name).AddComponent<T>();
        newManager.transform.SetParent(transform);
        newManager.Initialize(Settings);

        m_Managers.Add(newManager);
    }
}