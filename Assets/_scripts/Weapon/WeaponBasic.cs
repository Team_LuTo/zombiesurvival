﻿using UnityEngine;

public class WeaponBasic : Weapon
{
    [CreateAssetMenu(fileName = "WeaponBasic", menuName = "Game/Weapon/Basic")]
    public class Data : DataBase
    {
        public float ReloadSpeed;
        public int MaxBullets;

        public float FireRate;
        public float Range;
        
        public int Spread;
        public int Damage;

        public override Weapon CreateNewWeapon()
        {
            return new WeaponBasic(this);
        }
    }

    //Local weapon data
    public float CurReloadTime;
    public int CurBullets;
    public float CurFireTime;

    //Global weapon data
    private Data m_Data;

    //Create new weapon using supplied data
    public WeaponBasic(Data _Data)
    {
        m_Data = _Data;

        CurBullets = m_Data.MaxBullets;
    }

    //Firetime and Reload time
    public override void Update()
    {
        base.Update();

        CurFireTime = Mathf.Max(CurFireTime - Time.deltaTime, 0);

        if (IsReloading)
        {
            CurReloadTime -= Time.deltaTime;

            if (CurReloadTime <= 0)
            {
                IsReloading = false;
                CurBullets = m_Data.MaxBullets;
            }
        }
    }

    public override void PrimaryFire()
    {
        base.PrimaryFire();

        if (!IsReloading)
        {
            if (CurBullets == 0)
            {
                Reload();
            }
            else
            {
                if (CurFireTime <= 0)
                {
                    --CurBullets;

                    RaycastHit2D hit = Physics2D.Raycast(m_Owner.transform.position, m_Owner.transform.forward, m_Data.Range, LayerMask.GetMask("Unit"));

                    if (hit.collider != null)
                    {
                        DrawLine(m_Owner.transform.position, hit.point, Color.magenta);
                        hit.collider.gameObject.GetComponent<Zombie>().TakeDamage(m_Data.Damage);
                    }
                    else
                    {
                        DrawLine(m_Owner.transform.position, m_Owner.transform.forward * m_Data.Range, Color.magenta);
                    }

                    CurFireTime = m_Data.FireRate;
                }
            }
        }
    }

    public override void SecondaryFire()
    {
        base.SecondaryFire();

        Debug.Log("SecondaryFire");
    }

    public override void Reload()
    {
        if (IsReloading || CurBullets == m_Data.MaxBullets)
            return;

        base.Reload();

        CurReloadTime = m_Data.ReloadSpeed;
        IsReloading = true;

        Debug.Log("Reload");
    }

    void DrawLine(Vector2 start, Vector2 end, Color color, float duration = 0.2f)
    {
        LineRenderer lr = new GameObject("Bullet Trail").AddComponent<LineRenderer>();
        lr.transform.position = start;

        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startColor = color;
        lr.endColor = color;

        lr.sortingOrder = 2;

        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;

        lr.SetPosition(0, start);
        lr.SetPosition(1, end);

        GameObject.Destroy(lr.gameObject, duration);
    }
}
