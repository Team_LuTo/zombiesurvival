﻿using UnityEngine;

public class PlayerManager : Manager
{
    [System.Serializable]
    public class Data
    {
        public PlayerController PlayerPrefab;
    }

    private Data m_Data;

    public PlayerController MainPlayer { get; private set; }

    public override void Initialize(object Settings)
    {
        m_Data = (Data)Settings;

        MainPlayer = Instantiate(m_Data.PlayerPrefab, Vector3.zero, Quaternion.identity);
    }
}
