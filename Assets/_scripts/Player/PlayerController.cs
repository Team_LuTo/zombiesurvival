﻿using UnityEngine;

public class PlayerController : Unit
{
    public class PlayerStates
    {
        public PlayerIdleState IdleState;
        public PlayerRunningState RunningState;
    }

    //Weapon stuff
    private WeaponHandler _weaponHandler;
    [SerializeField] private Weapon.DataBase _testWeaponTemplate;
    [SerializeField] private Weapon _testWeapon;

    [HideInInspector] public Vector2 LookDir;

    public PlayerStates States = new PlayerStates();

    private void Awake()
    {
        //Initialize weapon stuff
        _testWeapon = _testWeaponTemplate.CreateNewWeapon();
        _testWeapon.SetOwner(this);

        _weaponHandler = GetComponent<WeaponHandler>();
        _weaponHandler.Initialize(this);
        _weaponHandler.EquipWeapon(_testWeapon);

        //Initialize states
        States.IdleState = new PlayerIdleState(this);
        States.RunningState = new PlayerRunningState(this);
        ChangeState(States.IdleState);

        //Initialize data
        unitProperties.Health = unitProperties.MaxHealth;

        unitMovement = GetComponent<UnitMovement>();
    }

    private void Start()
    {
        _healthBar = Instantiate(HealthBarPrefab, transform) as Healthbar;
        _healthBar.SetTarget(this);
       
    }

    public override void Update()
    {
        base.Update();

        if (unitProperties.Dead) return;

        LookDir = Input.mousePosition;
        Vector2 objPos = Camera.main.WorldToScreenPoint(transform.position);
        LookDir -= objPos;
        float angle = Mathf.Atan2(LookDir.y, LookDir.x) * Mathf.Rad2Deg;
        unitProperties.Sprite.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    public void PrimaryAction()
    {
        _weaponHandler.PrimaryFire();
    }

    public void Reload()
    {
        _weaponHandler.Reload();
    }

    public bool HandleMovement()
    {
        Vector2 moveDir = Vector2.zero;

        // Move Forwards
        if (Input.GetKey(KeyCode.W))
        {
            moveDir += Vector2.up;
        }
        // Move Backwards
        if (Input.GetKey(KeyCode.S))
        {
            moveDir += Vector2.down;
        }
        // Move Left
        if (Input.GetKey(KeyCode.A))
        {
            moveDir += Vector2.left;
        }
        // Move Right
        if (Input.GetKey(KeyCode.D))
        {
            moveDir += Vector2.right;
        }

        if (moveDir == Vector2.zero)
            return false;

        unitMovement?.Move(moveDir);
        return true;
    }

    public bool HandleReload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
            return true;
        }

        return false;
    }

    public override void TakeDamage(int damage)
    {
       base.TakeDamage(damage);

    }
}
