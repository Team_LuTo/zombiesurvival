﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRunningState : Unit.UnitState
{
    private PlayerController _player;
    public PlayerRunningState(PlayerController player)
    {
        _player = player;
    }

    void Start()
    {
        
    }

    public override void OnStateEnter() {}

    public override void OnStateExit() {}

    public override void Update()
    {
        if (!_player.HandleMovement())
            _player.ChangeState(_player.States.IdleState);
        else
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                _player.PrimaryAction();
            }

            if (_player.HandleReload())
            {
                //TODO: ChangeState to PlayerReloadState
            }
        }
    }
}
