﻿using UnityEngine;

public abstract class Weapon
{
    public abstract class DataBase : ScriptableObject
    {
        public abstract Weapon CreateNewWeapon();
    }

    public bool IsReloading { get; protected set; }
    protected Unit m_Owner { get; private set; }

    public void SetOwner(Unit Owner) { m_Owner = Owner; }

    public virtual void OnEquip() { Debug.Log("Equipped " + GetType().Name); }
    public virtual void OnUnequip() { Debug.Log("Unequipped " + GetType().Name); }

    public virtual void PrimaryFire() { }
    public virtual void SecondaryFire() { }

    public virtual void Reload() { }

    public virtual void Update() { }
}
