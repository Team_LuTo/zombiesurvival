﻿using UnityEngine;

public class WeaponHandler : MonoBehaviour
{
    private Unit m_Owner;

    private Weapon m_CurrentWeapon;

    public void Initialize(Unit Owner)
    {
        m_Owner = Owner;

        m_CurrentWeapon = null;
    }

    public void EquipWeapon(Weapon W)
    {
        if (!m_Owner)
        {
            Debug.LogError("<color=red>\"" + gameObject.name + "\"</color> tried to equip a weapon but WeaponHandler Owner does not exist.");
            return;
        }

        //Unequip old weapon
        if (m_CurrentWeapon != null)
            m_CurrentWeapon.OnUnequip();

        //Equip new weapon
        m_CurrentWeapon = W;
        m_CurrentWeapon.OnEquip();
    }

    private void Update()
    {
        if (m_CurrentWeapon != null)
        {
            m_CurrentWeapon.Update();
        }
    }

    public void PrimaryFire()
    {
        if (m_CurrentWeapon != null)
            m_CurrentWeapon.PrimaryFire();
    }

    public void SecondaryFire()
    {
        if (m_CurrentWeapon != null)
            m_CurrentWeapon.SecondaryFire();
    }

    public void Reload()
    {
        if (m_CurrentWeapon != null)
            m_CurrentWeapon.Reload();
    }

    public bool IsReloading()
    {
        if (m_CurrentWeapon == null)
            return false;

        return m_CurrentWeapon.IsReloading;
    }
}
