﻿using UnityEngine;

public class AudioManager : Manager
{
    [System.Serializable]
    public class Data
    {
        [Header("Sliders"), Range(0, 1)]
        public float MasterVolume = 1.0f;

        [Range(0, 1)]
        public float SoundVolume = 1.0f;

        [Range(0, 1)]
        public float MusicVolume = 1.0f;
    }

    private Data m_Data;

    public override void Initialize(object Settings)
    {
        m_Data = (Data)Settings;
    }

    //WIP
}
