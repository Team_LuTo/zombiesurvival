﻿using UnityEngine;

//Dunno om jag gillar MovementProperties som namn, du får bestämma du
//Det är iaf data som MovementSpeed, Turnrate, etc
[CreateAssetMenu(fileName = "MovementProperties", menuName ="Game/Properties/Movement")]
public class MovementProperties : ScriptableObject
{
    public float MovementSpeed;
    public float TurnRate;
}

public class UnitMovement : MonoBehaviour
{
    public enum MovementActionStatus
    {
        None,
        Rotating,
        Moving
    }

    [SerializeField]
    private MovementProperties _properties;

    //Per-frame based movement using a vector
    public virtual MovementActionStatus Move(Vector3 dir)
    {
        transform.Translate(dir.normalized * _properties.MovementSpeed * Time.deltaTime);
        return MovementActionStatus.None;
    }
}
