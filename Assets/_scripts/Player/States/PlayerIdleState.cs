﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : Unit.UnitState
{
    private readonly PlayerController _player;

    public PlayerIdleState(PlayerController player)
    {
        _player = player;
    }

    public override void OnStateEnter() {}

    public override void OnStateExit() {}

    public override void Update()
    {
        if(_player.HandleMovement())
            _player.ChangeState(_player.States.RunningState);
        else
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                _player.PrimaryAction();
            }

            if (_player.HandleReload())
            {
                //TODO: ChangeState to PlayerReloadState
            }
        }
    }
}
