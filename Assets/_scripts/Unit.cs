﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitMovement))]
public class Unit : MonoBehaviour
{
    public abstract class UnitState
    {
        public abstract void OnStateEnter();
        public abstract void OnStateExit();

        public abstract void Update();
    }

    [System.Serializable]
    public class UnitProperties
    {
        [Header("Defensive")]
        public int MaxHealth;
        public int Health;
        public int Armor;

        //[Header("Movement")]
        //public float MovementSpeed;

        [Header("Visuals")]
        public SpriteRenderer Sprite;

        [Space(10)] public bool Dead;
    }

    [SerializeField]
    protected UnitProperties unitProperties;

    protected UnitMovement unitMovement;

    private UnitState _currentState;

    public Healthbar HealthBarPrefab;
    protected Healthbar _healthBar;

    public virtual void Update()
    {
        if (unitProperties.Dead) return;

        _currentState?.Update();
    }

    public void ChangeState(UnitState state)
    {
        Debug.Log("Changing state \"" + _currentState?.GetType().FullName + "\" to \"" + state.GetType().FullName + "\"");
        _currentState?.OnStateExit();

        _currentState = state;
        _currentState.OnStateEnter();
    }

    public virtual void TakeDamage(int damage)
    {
        if (unitProperties.Dead)
            return;

        unitProperties.Health -= damage;

        if (unitProperties.Health <= 0)
        {
            unitProperties.Dead = true;
        }
    }

    public float GetHealthPercent()
    {
        return (float)unitProperties.Health / unitProperties.MaxHealth;
    }
}
